"""Simple FASTAPI App for workshop purposes

Returns:
  None
"""

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from ska_ems_webhook import config  # noqa: F401  # pylint: disable=W0611
from ska_ems_webhook.controller import cmms, prts

app = FastAPI(
    title="Engineering Management System (EMS) Webhooks",
    summary="""
    This API is used to provide webhook access to the Engineering Management
    System (EMS)""",
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(cmms.router)
app.include_router(prts.router)


@app.get("/")
async def root():
    """Simple FASTAPI App for workshop purposes"""
    return {"message": "Hello World"}
