Engineering Management System (EMS) Webhook
===========================================

The SKA EMS Webhooks are publicly accessible endpoints designed to facilitate communication with the EMS API Gateway. 
These webhooks enable external systems and applications to send real-time data and event notifications directly to the EMS infrastructure, 
ensuring timely and efficient data exchange. This setup allows the SKA EMS to receive and process incoming information from various sources, 
enhancing the overall responsiveness and functionality of the system.


.. HOME SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 1
  :caption: Table of Contents
  :hidden:

  restapi
