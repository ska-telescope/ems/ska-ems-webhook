"""
Test the PRTS webhook endpoints
"""

import pytest
from fastapi.testclient import TestClient

from ska_ems_webhook.main import app
from ska_ems_webhook.models.schemas import Ticket

client = TestClient(app)


@pytest.mark.asyncio
async def test_prts_webhook(mocker):
    """
    Test SPRTS Webhook endpoint.
    """
    mocker.patch(
        "ska_ems_webhook.controller.prts.create_work_order",
        return_value={"taskID": 12345},
    )
    mocker.patch(
        "ska_ems_webhook.controller.prts.create_work_order_comment",
        return_value=None,
    )
    mocker.patch(
        "ska_ems_webhook.controller.prts.create_ticket_work_order_link",
        return_value=None,
    )

    ticket = Ticket(
        action="CREATE WORK ORDER",
        key="TEST-123",
        summary="Test Summary",
        description="Test Description",
        location_id="1",
        part_number="PN1",
        serial_number="SN1",
        scheduled_date="2023-10-10",
    )

    response = client.post(
        "/jira/sprts",
        json=ticket.dict(),
    )

    assert response.status_code == 201
    assert response.json() == {"taskID": 12345}
