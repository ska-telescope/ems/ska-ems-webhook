"""
Test the api_gateway_cmms module methods
"""

import pytest

from ska_ems_webhook.services.api_gateway_prts import (
    create_ticket_work_order_link,
    delete_ticket_work_order,
    get_ticket_work_orders,
    update_ticket_work_order,
)


@pytest.mark.asyncio
async def test_retrieves_work_orders_with_valid_ticket_key(mocker):
    """
    Test Get JIRA SPRTS Ticket - Work Order links through EMS API Gateway.
    """
    ticket_key = "valid_ticket_key"
    expected_work_orders = [
        {"id": 1, "name": "Work Order 1"},
        {"id": 2, "name": "Work Order 2"},
    ]
    mock_response = mocker.Mock()
    mock_response.status_code = 200
    mock_response.json.return_value = expected_work_orders
    mocker.patch("requests.get", return_value=mock_response)

    result = await get_ticket_work_orders(ticket_key)

    assert result == expected_work_orders


@pytest.mark.asyncio
async def test_create_work_order_link_success(mocker):
    """
    Test Create JIRA SPRTS Ticket - Work Order links through EMS API Gateway.
    """
    mock_response = mocker.Mock()
    mock_response.status_code = 201
    mock_response.json.return_value = {"success": True}
    mocker.patch("requests.post", return_value=mock_response)

    result = await create_ticket_work_order_link(
        "TICKET-123", 456, 789, "Test Summary"
    )

    assert result == {"success": True}


@pytest.mark.asyncio
async def test_successful_update(mocker):
    """
    Test Update JIRA SPRTS Ticket - Work Order links through EMS API Gateway.
    """
    ticket_key = "valid_ticket_key"
    payload = {"field": "value"}
    mock_response = mocker.Mock()
    mock_response.status_code = 204
    mocker.patch("requests.put", return_value=mock_response)

    await update_ticket_work_order(ticket_key, payload)


@pytest.mark.asyncio
async def test_successful_deletion(mocker):
    """
    Test Delete JIRA SPRTS Ticket - Work Order links through EMS API Gateway.
    """
    mock_response = mocker.Mock()
    mock_response.status_code = 204
    mocker.patch("requests.delete", return_value=mock_response)

    ticket_key = "valid_ticket_key"
    ticket_work_order_id = 123

    await delete_ticket_work_order(ticket_key, ticket_work_order_id)
