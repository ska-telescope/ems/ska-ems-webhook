"""
This module provides methods to handle API operations with external services,
such as Limble.
"""

import json
import logging
import os

import requests

from ska_ems_webhook.utils.api_requests import get_request
from ska_ems_webhook.utils.exception_handler import (
    handle_request_exceptions,
    handle_response,
)

EMS_GATEWAY_TOKEN = os.getenv("EMS_GATEWAY_TOKEN")
EMS_GATEWAY_URL = os.getenv("EMS_GATEWAY_URL")

headers = {"Authorization": f"Bearer {EMS_GATEWAY_TOKEN}"}


@handle_request_exceptions
async def get_ticket_work_orders(ticket_key):
    """
    Get the work orders associated with the provided ticket key.

    Args:
        - ticket_key (str): The ID of the ticket for which work orders need to
          be retrieved.

    Returns:
        - list: A list of dictionaries, each representing a work order that
          matches the provided ticket_key.
    """

    api_url = f"{EMS_GATEWAY_URL}/jira/tickets/{ticket_key}/work-orders"
    params = {}

    request = get_request(api_url, params, headers)
    response = request.make_request()

    messages = {"success": "Successfully retrieved ticket work order links"}
    return handle_response(response, messages, return_json=True)


@handle_request_exceptions
async def create_ticket_work_order_link(
    ticket_key, work_order_id, location_id, summary
):
    """
    Create a Work Order link on the JIRA ticket.

    Args:
        - ticket_key (str): The ID of the ticket for which the work order link
          is to be created.
        - work_order_id (int): The ID of the work order to be linked with the
          ticket.
        - location_id (int): The ID of the location associated with the task.
        - summary (str): A brief summary or description of the task.

    Returns:
        dict: A dictionary containing the response JSON of the request to
        create the ticket work order link.
    """
    api_url = f"{EMS_GATEWAY_URL}/jira/tickets/{ticket_key}/work-order"

    payload = json.dumps(
        {
            "title": f"Task #{work_order_id}",
            "summary": summary,
            "url": f"https://app.limblecmms.com/"
            f"task/{work_order_id}/{location_id}",
            "resolved": False,
        }
    )
    response = requests.post(
        api_url, headers=headers, data=payload, timeout=10
    )

    if response.status_code == 201:
        logging.info("Successfully created remote link for %s", ticket_key)
        return response.json()

    handle_response(response)
    return None


@handle_request_exceptions
async def update_ticket_work_order(ticket_key, payload):
    """
    Update a ticket work order.

    Args:
        - ticket_key (str): The ID of the ticket associated with the work order
          to be updated.
        - payload (dict): A dictionary containing the JSON payload of the
          request, specifying the fields and values to be updated in the work
          order.
    """
    api_url = f"{EMS_GATEWAY_URL}/jira/tickets/{ticket_key}/work-order"

    payload = json.dumps(payload)

    response = requests.put(api_url, headers=headers, data=payload, timeout=10)

    if response.status_code == 204:
        logging.info("Successfully updated remote link for %s", ticket_key)
    else:
        handle_response(response)


@handle_request_exceptions
async def delete_ticket_work_order(ticket_key, ticket_work_order_id):
    """
    Delete a ticket work order

    Args:
        - ticket_key (str): The ID of the ticket associated with the work order
          to be deleted.
        - ticket_workorder_id (int): The ID of the work order to be deleted.
    """
    api_url = f"{EMS_GATEWAY_URL}/jira/tickets/{ticket_key}/work-order"

    params = {"link_id": ticket_work_order_id}

    response = requests.delete(
        api_url, headers=headers, params=params, timeout=20
    )

    if response.status_code == 204:
        logging.info("Successfully removed remote link for %s", ticket_key)
    else:
        handle_response(response)
