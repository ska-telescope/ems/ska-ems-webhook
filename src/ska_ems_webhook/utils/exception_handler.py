"""
exception_.py

This module provides methods to handle request exceptions.
"""

import logging
from functools import wraps

import requests
from fastapi import HTTPException


def handle_request_exceptions(func):
    """
    Wrap a function in a try-except block to handle request exceptions.

    Args:
        func (function): The function to be wrapped by the decorator.

    Returns:
        function: The wrapped function with exception handling.

    Raises:
        Exception: Any exceptions raised by the wrapped function will be caught
        and handled within the decorator, logging and raising the error.
    """

    @wraps(func)
    async def wrapper(*args, **kwargs):
        try:
            return await func(*args, **kwargs)
        except requests.exceptions.Timeout as e:
            logging.error("Request timed out: %s", e)
            raise HTTPException(
                status_code=504, detail="Request timed out"
            ) from e
        except requests.exceptions.ConnectionError as e:
            logging.error("Connection error occurred: %s", e)
            raise HTTPException(
                status_code=502, detail="Connection error"
            ) from e
        except requests.exceptions.HTTPError as e:
            logging.error("HTTP error occurred: %s", e)
            raise HTTPException(status_code=404, detail="HTTP error") from e
        except requests.exceptions.RequestException as e:
            logging.error("Request exception occurred: %s", e)
            raise HTTPException(
                status_code=500, detail="Request exception"
            ) from e

    return wrapper


def handle_response(
    response,
    messages=None,
    status_code=200,
    return_json=False,
):
    """
    Handle unexpected responses
    """
    messages = messages or {}

    def log_success_message():
        if "success" in messages:
            logging.info(messages["success"])

    if response.status_code == status_code:
        log_success_message()
        if return_json:
            return response.json()

        return response
    if response.status_code == 401:
        logging.error("401 Unauthorized Error! Check your credentials.")
        return None

    error_message = messages.get("error", "Unexpected response.")
    logging.error("%s Status code: %s", error_message, response.status_code)
    return None
