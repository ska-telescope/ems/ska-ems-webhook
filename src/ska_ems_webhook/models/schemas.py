"""
Pydantic models
"""

from typing import Optional

from pydantic import BaseModel


# pylint: disable=too-few-public-methods
class Task(BaseModel):
    """
    A Pydantic model representing a Limble task.
    """

    taskID: int
    status: Optional[str] = None
    category: Optional[str] = None
    user: Optional[str] = None


class Ticket(BaseModel):
    """
    A Pydantic model representing a JIRA SPRTS ticket.

    Attributes:
        - action (str): The action associated with the ticket.
        - key (str): A unique key associated with the ticket.
        - summary (str): A brief summary of the ticket.
        - description (str): Detailed description of the ticket.
        - locationId (int): The ID of the location where the ticket is related.
        - partNumber (str): The part number associated with the ticket.
        - serialNumber (str): The serial number related to the ticket.
    """

    action: str
    key: Optional[str] = None
    summary: Optional[str] = None
    description: Optional[str] = None
    location_id: Optional[str] = None
    part_number: Optional[str] = None
    serial_number: Optional[str] = None
    scheduled_date: Optional[str] = None
    remote_issue_link_id: Optional[int] = None
