"""
Tests the handle_request_exceptions decorator.
"""

from unittest.mock import MagicMock, patch

import pytest
import requests
from fastapi import HTTPException

from ska_ems_webhook.utils.exception_handler import (
    handle_request_exceptions,
    handle_response,
)


@pytest.mark.asyncio
async def test_handle_timeout_exception():
    """
    Test the handle_request_exceptions decorator handles Timeout exceptions.
    """

    def mock_func():
        raise requests.exceptions.Timeout("Request timed out")

    decorated_func = handle_request_exceptions(mock_func)

    with pytest.raises(HTTPException) as exc_info:
        await decorated_func()

    assert exc_info.value.status_code == 504
    assert exc_info.value.detail == "Request timed out"


@pytest.mark.asyncio
async def test_handle_request_exceptions_connection_error():
    """
    Test the handle_request_exceptions decorator handles ConnectionError
    exceptions.
    """

    def mock_func():
        raise requests.exceptions.ConnectionError("Connection error occurred")

    decorated_func = handle_request_exceptions(mock_func)

    with pytest.raises(HTTPException) as exc_info:
        await decorated_func()

    assert exc_info.value.status_code == 502
    assert exc_info.value.detail == "Connection error"


@pytest.mark.asyncio
async def test_handle_request_exceptions_http_error():
    """
    Test the handle_request_exceptions decorator handles HTTPError exceptions.
    """

    def mock_func():
        raise requests.exceptions.HTTPError()

    decorated_func = handle_request_exceptions(mock_func)

    with pytest.raises(HTTPException) as exc_info:
        await decorated_func()

    assert exc_info.value.status_code == 404
    assert exc_info.value.detail == "HTTP error"


@pytest.mark.asyncio
async def test_handle_request_exceptions_request_exception():
    """
    Test the handle_request_exceptions decorator handles general
    RequestException.
    """

    def mock_func():
        raise requests.exceptions.RequestException(
            "Request exception occurred"
        )

    decorated_func = handle_request_exceptions(mock_func)

    with pytest.raises(HTTPException) as exc_info:
        await decorated_func()

    assert exc_info.value.status_code == 500
    assert exc_info.value.detail == "Request exception"


@patch("ska_ems_webhook.utils.exception_handler.logging.error")
def test_handle_response_unauthorized(mock_logging_error):
    """
    Test the handle_response function handles 401 Unauthorized responses.
    """
    mock_response = MagicMock()
    mock_response.status_code = 401

    handle_response(mock_response)

    mock_logging_error.assert_called_once_with(
        "401 Unauthorized Error! Check your credentials."
    )
