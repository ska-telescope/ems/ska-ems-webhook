"""
This module provides a function to handle incoming jira tickets webhooks.
The main function is `ticket_webhook`, which processes HTTP POST requests
based on the action required and the payload provided.
"""

import logging
from datetime import datetime

from fastapi import APIRouter
from fastapi.responses import JSONResponse

from ska_ems_webhook.models.schemas import Ticket
from ska_ems_webhook.services.api_gateway_cmms import (
    create_work_order,
    create_work_order_comment,
)
from ska_ems_webhook.services.api_gateway_prts import (
    create_ticket_work_order_link,
)
from ska_ems_webhook.services.database import delete_mapping
from ska_ems_webhook.utils.exception_handler import handle_request_exceptions

router = APIRouter()


@router.post("/jira/sprts")
@handle_request_exceptions
async def ticket_webhook(ticket: Ticket):
    """
    Webhook endpoint to handle Jira requests.
    """
    response = []

    logging.info("JIRA webhook received")
    logging.info("Payload: %s", ticket)

    if ticket.action == "DELETE REMOTE ISSUE LINK":
        remote_issue_link_id = ticket.remote_issue_link_id
        delete_mapping(None, remote_issue_link_id)
    elif ticket.action == "CREATE WORK ORDER":
        key = ticket.key
        summary = ticket.summary
        description = ticket.description
        location_id = ticket.location_id
        scheduled_date = (
            int(
                datetime.strptime(
                    ticket.scheduled_date, "%Y-%m-%d"
                ).timestamp()
            )
            if ticket.scheduled_date
            else None
        )

        # Create Task in Limble
        response = await create_work_order(
            key, summary, description, location_id, scheduled_date
        )

        if response is not None:
            # Create Task Comment in Limble
            jira_url = f"https://jira.skatelescope.org/browse/{key}"
            await create_work_order_comment(
                response["taskID"],
                f"""Task created by the
                 <a href={jira_url} target='_blank'>{key}</a>""",
            )

            # Create web link on SPRTS ticket
            await create_ticket_work_order_link(
                key, response["taskID"], location_id, summary
            )

    return JSONResponse(content=response if response else {}, status_code=201)
