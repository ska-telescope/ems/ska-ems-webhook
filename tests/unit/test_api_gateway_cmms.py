"""
Test the api_gateway_cmms module methods
"""

import pytest

from ska_ems_webhook.services.api_gateway_cmms import (
    create_work_order,
    create_work_order_comment,
    get_task,
)


@pytest.mark.asyncio
async def test_get_task_successfully(mocker):
    """
    Test Get Limble Task through EMS API Gateway.
    """
    mock_response = mocker.Mock()
    mock_response.status_code = 200
    mock_response.json.return_value = {"id": 1, "name": "Test Task"}

    mocker.patch("requests.get", return_value=mock_response)

    task_id = 1
    result = await get_task(task_id)

    assert result == {"id": 1, "name": "Test Task"}


@pytest.mark.asyncio
async def test_create_work_order_success(mocker):
    """
    Test Create Limble Task through EMS API Gateway.
    """
    mock_response = mocker.Mock()
    mock_response.status_code = 201
    mock_response.json.return_value = {"task_id": 123}
    mocker.patch("requests.post", return_value=mock_response)

    result = await create_work_order(
        "key123", "Test Summary", "Test Description", 1, 1622548800
    )

    assert result == {"task_id": 123}


@pytest.mark.asyncio
async def test_create_comment_success(mocker):
    """
    Test Create Limble Task Comment through EMS API Gateway.
    """
    mock_response = mocker.Mock()
    mock_response.status_code = 201
    mock_response.json.return_value = {"message": "Comment created"}
    mocker.patch("requests.post", return_value=mock_response)

    task_id = 123
    comment = "This is a test comment"
    result = await create_work_order_comment(task_id, comment)

    assert result == {"message": "Comment created"}
