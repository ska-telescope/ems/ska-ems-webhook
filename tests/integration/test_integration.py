"""
Simple FASTAPI App Tests for workshop purposes

Returns:
  None
"""

import pytest
import requests


@pytest.mark.post_deployment
def test_root_endpoint():
    """
    Tests the root endpoint of the deployed FastAPI application.

    Sends a GET request to the root endpoint and verifies
    the response matches the expected status code and message.
    """
    # URL of the service, assuming the service is exposed externally
    url = "http://test-ska-ems-webhook:80/"

    # Sending a GET request to the root endpoint
    response = requests.get(url, timeout=10)

    # Verifying the status code
    assert (
        response.status_code == 200
    ), f"Expected status code 200, got {response.status_code}"

    # Verifying the response body
    expected_message = {"message": "Hello World"}
    assert (
        response.json() == expected_message
    ), f"Expected message {expected_message}, got {response.json()}"
