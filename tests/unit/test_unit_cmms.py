"""
Test the CMMS webhook endpoints
"""

import pytest
from fastapi.testclient import TestClient

from ska_ems_webhook.main import app

client = TestClient(app)


@pytest.mark.asyncio
async def test_cmms_webhook(mocker):
    """
    Test Limble Webhook endpoint.
    """
    mock_get_mapping = mocker.patch(
        "ska_ems_webhook.controller.cmms.get_mapping",
        return_value={"jira_key": "TEST-123", "jira_link_id": 456},
    )
    mock_get_ticket_work_orders = mocker.patch(
        "ska_ems_webhook.controller.cmms.get_ticket_work_orders",
        return_value=[{"id": 456, "resolved": False}],
    )
    mock_update_ticket_work_order = mocker.patch(
        "ska_ems_webhook.controller.cmms.update_ticket_work_order"
    )
    # mock_delete_ticket_work_order = mocker.patch(
    #     "ska_ems_webhook.controller.cmms.delete_ticket_work_order"
    # )
    # mock_delete_mapping = mocker.patch(
    #     "ska_ems_webhook.controller.cmms.delete_mapping"
    # )

    task_payload = {"taskID": 1, "status": "COMPLETE"}

    response = client.post(
        "/cmms/task",
        json=task_payload,
    )

    assert response.status_code == 200
    assert response.json() == {"message": "Task #1 updated"}

    mock_get_mapping.assert_called_once_with(1)
    mock_get_ticket_work_orders.assert_called_once_with("TEST-123")
    mock_update_ticket_work_order.assert_called_once_with(
        "TEST-123", {"id": 456, "resolved": True}
    )
