"""
Initialize database connections
"""

import logging
import os

import psycopg2

EMS_POSTGRESS_HOST = os.getenv("EMS_POSTGRESS_HOST")
EMS_POSTGRESS_PORT = os.getenv("EMS_POSTGRESS_PORT")
EMS_POSTGRESS_DATABASE = os.getenv("EMS_POSTGRESS_DATABASE")
EMS_POSTGRESS_USERNAME = os.getenv("EMS_POSTGRESS_USERNAME")
EMS_POSTGRESS_PASSWORD = os.getenv("EMS_POSTGRESS_PASSWORD")


def create_ems_db_connection():
    """
    Create a connection to the EMS database.

    Returns:
        psycopg2 connection object: A connection to the PostgreSQL database if
        successful, None otherwise.
    """
    try:
        conn = psycopg2.connect(
            host=EMS_POSTGRESS_HOST,
            port=EMS_POSTGRESS_PORT,
            dbname=EMS_POSTGRESS_DATABASE,
            user=EMS_POSTGRESS_USERNAME,
            password=EMS_POSTGRESS_PASSWORD,
        )
        return conn
    except psycopg2.Error as e:
        logging.error({"An error creating a connection to postgress": str(e)})
        return None
