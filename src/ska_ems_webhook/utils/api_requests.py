"""
This module provides a utility function to make HTTP requests.
"""

from dataclasses import dataclass
from typing import Any, Optional

import requests


@dataclass
class ApiRequest:
    """
    Dataclass object for an API request.
    """

    # pylint: disable=too-many-instance-attributes
    api_url: str
    headers: Optional[dict[str, str]] = None
    username: Optional[str] = None
    password: Optional[str] = None
    payload: Optional[str] = None
    params: Optional[dict[str, int | Any]] = None
    method: str = "POST"
    timeout: int = 10

    def make_request(self):
        """
        Make an HTTP request.

        Returns:
        - Response: The HTTP response object.

        Raises:
        - ValueError: If an unsupported HTTP method is provided.
        """
        method = self.method.upper()

        if method == "GET":
            return requests.get(
                self.api_url,
                headers=self.headers,
                # auth=HTTPBasicAuth(self.username, self.password),
                params=self.params,
                timeout=self.timeout,
            )
        raise ValueError(f"Unsupported HTTP method: {method}")


def get_request(api_url, params, headers):
    """
    Generic Get request
    """
    return ApiRequest(
        api_url=api_url,
        params=params,
        headers=headers,
        method="GET",
    )
