"""
This module provides a function to handle incoming Work Order webhooks.
The main function is `work_order_webhook`, which processes HTTP POST requests
and updates the work orders in the database.
"""

import logging

from fastapi import APIRouter
from fastapi.responses import JSONResponse

from ska_ems_webhook.models.schemas import Task
from ska_ems_webhook.services.api_gateway_cmms import get_task
from ska_ems_webhook.services.api_gateway_prts import (
    delete_ticket_work_order,
    get_ticket_work_orders,
    update_ticket_work_order,
)
from ska_ems_webhook.services.database import delete_mapping, get_mapping
from ska_ems_webhook.utils.exception_handler import handle_request_exceptions

router = APIRouter()


@router.post("/cmms/task")
@handle_request_exceptions
async def task_webhook(task: Task):
    """
    Webhook endpoint to update work orders.
    """
    logging.info("Limble webhook received")
    logging.info("Payload: %s", task)

    task_id = task.taskID

    # Check if task link exists in database else no action should be
    # taken for this task
    jira_limble_link = get_mapping(task_id)
    if not jira_limble_link:
        return JSONResponse(
            content={"message": f"Task #{task_id} no action required"},
            status_code=200,
        )

    ticket_key = jira_limble_link["jira_key"]
    ticket_link_id = jira_limble_link["jira_link_id"]

    remote_links = await get_ticket_work_orders(ticket_key)
    if not remote_links:
        return

    message = ""
    for remote_link in remote_links:
        if remote_link["id"] == ticket_link_id:
            if task.status == "DELETED":
                await delete_ticket_work_order(ticket_key, remote_link["id"])
                delete_mapping(task_id)
                message = f"Task #{task_id} deleted"
            elif task.status == "COMPLETE":
                remote_link["resolved"] = True
                await update_ticket_work_order(ticket_key, remote_link)
                message = f"Task #{task_id} updated"
            elif task.status == "COMPLETED TASK REOPENED":
                remote_link["resolved"] = False
                await update_ticket_work_order(ticket_key, remote_link)
                message = f"Task #{task_id} updated"
            elif task.status == "CHANGED TASK NAME":
                tasks = await get_task(task_id)
                if tasks:
                    remote_link["summary"] = tasks[0]["name"]
                    await update_ticket_work_order(ticket_key, remote_link)
                    message = f"Task #{task_id} updated"

    return JSONResponse(
        content={"message": message},
        status_code=200,
    )
