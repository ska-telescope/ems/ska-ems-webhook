"""
Database queries
"""

import logging

import psycopg2

from ska_ems_webhook.utils.database import create_ems_db_connection


def get_mapping(task_id):
    """
    Get Jira - Limble mapping from EMS database.

    Args:
        task_id (str): The ID of the Limble task.

    Returns:
        dict or None: A dictionary containing 'jira_key' and 'jira_link_id'.
    """
    sql_select_query = """
        SELECT jira_key, jira_link_id
        FROM prts_issue_work_orders
        WHERE limble_task_id = %s
    """

    try:
        with create_ems_db_connection() as connection:
            with connection.cursor() as cursor:
                cursor.execute(sql_select_query, (task_id,))
                result = cursor.fetchone()

            if result:
                return {
                    "jira_key": result[0],
                    "jira_link_id": result[1],
                }
            return None
    except psycopg2.Error as e:
        logging.error("Error fetching mapping for task_id %s: %s", task_id, e)
        return None


def delete_mapping(task_id=None, jira_link_id=None):
    """
    Delete Jira - Limble mapping from the EMS database.

    Args:
        task_id (str): The ID of the Limble task.
        jira_link_id (str, optional): The Jira link ID to delete.

    Returns:
        bool: True if rows were deleted, False otherwise.
    """
    if jira_link_id:
        sql_delete_query = (
            "DELETE FROM prts_issue_work_orders WHERE jira_link_id = %s"
        )
        params = (jira_link_id,)
        delete_target = f"Jira Link ID #{jira_link_id}"
    else:
        sql_delete_query = (
            "DELETE FROM prts_issue_work_orders WHERE limble_task_id = %s"
        )
        params = (task_id,)
        delete_target = f"Task ID #{task_id}"

    try:
        with create_ems_db_connection() as connection:
            with connection.cursor() as cursor:
                cursor.execute(sql_delete_query, params)
                rows_deleted = cursor.rowcount

            connection.commit()

        if rows_deleted:
            logging.info("Successfully deleted mapping for %s", delete_target)
            return True

        logging.info("No mapping found for %s", delete_target)
        return False

    except psycopg2.Error as e:
        logging.error("Error deleting mapping for %s: %s", delete_target, e)
        return False
