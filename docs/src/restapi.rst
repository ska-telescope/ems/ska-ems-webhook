REST API
=========

The EMS Webhooks Services are made accessible via a REST API. This is defined via an `OpenAPI Specification <https://swagger.io/specification/>`_

The default headers have been set to allow CORS requests to allow frontend and backend to communicate.
Individual endpoints
