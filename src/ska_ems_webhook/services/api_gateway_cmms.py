"""
This module provides methods to handle API operations with external services,
such as Limble and Jira.
"""

import json
import logging
import os

import requests

from ska_ems_webhook.utils.api_requests import get_request
from ska_ems_webhook.utils.exception_handler import (
    handle_request_exceptions,
    handle_response,
)

EMS_GATEWAY_TOKEN = os.getenv("EMS_GATEWAY_TOKEN")
EMS_GATEWAY_URL = os.getenv("EMS_GATEWAY_URL")
STFC_TOKEN = os.getenv("STFC_TOKEN")

headers = {
    "Authorization": f"Bearer {EMS_GATEWAY_TOKEN}",
    "Cookie": STFC_TOKEN,
}


@handle_request_exceptions
async def get_task(task_id):
    """
    Get a Limble task based on the provided task ID.

    Args:
        - task_id (int): The ID of the task to be retrieved.

    Returns:
        dict: A dictionary representing the task that matches the provided
        task_id.
    """
    api_url = f"{EMS_GATEWAY_URL}/limble/tasks?task_id={task_id}"
    params = {}

    request = get_request(api_url, params, headers)
    response = request.make_request()

    messages = {"success": "Successfully retrieved Limble task"}
    return handle_response(response, messages, return_json=True)


@handle_request_exceptions
async def create_work_order(
    key, summary, description, location_id, scheduled_date
):
    """
    Create a Limble Task.

    Args:
        - summary (str): A summary of the work order.
        - description (str): A detailed description of the work order.
        - location_id (int): The location id where the work order will be sent.
        - scheduled_date (int): The scheduled date (unix time) of the
          work order.

    Returns:
        dict: A dictionary containing the response JSON of the request to
        create the work order.
    """
    api_url = f"{EMS_GATEWAY_URL}/limble/tasks"
    payload = json.dumps(
        {
            "task_id": 0,
            "name": summary,
            "description": description,
            "location_id": location_id,
            "asset_id": 0,
            "status": 0,
            "priority": 0,
            "start_date": 0,
            "due_date": scheduled_date,
            "created_date": 0,
            "completed_date": 0,
            "last_edited_date": 0,
        }
    )
    response = requests.post(
        api_url, headers=headers, data=payload, timeout=10
    )

    if response.status_code == 201:
        logging.info("Successfully created work order for %s", key)
        return response.json()

    handle_response(response)
    return None


@handle_request_exceptions
async def create_work_order_comment(task_id, comment):
    """
    Create a Limble Task Comment.

    Args:
        - task_id (int): The id of the task.
        - comment (str): The comment that needs to be created.
    """

    api_url = f"{EMS_GATEWAY_URL}/limble/tasks/{task_id}/comment"
    params = {"comment": comment}
    response = requests.post(
        api_url, headers=headers, params=params, timeout=10
    )

    if response.status_code == 201:
        logging.info("Successfully created comment for Task #%s", task_id)
        return response.json()

    handle_response(response)
    return None
