# Use the official Python 3.10 image as the base image
FROM python:3.10-slim

# Update the package list and install apt utils to allow for package configuration during install
RUN apt-get update && \
    apt-get install -y apt-utils

# Install the required packages
RUN apt-get install -y --no-install-recommends \
    unattended-upgrades \
    && rm -rf /var/lib/apt/lists/*

# Apply security upgrades (base image)
RUN unattended-upgrade
RUN apt-get purge unattended-upgrades -y

# Install Poetry, a dependency management tool, in the container
RUN pip install "poetry<2.0.0" poetry-plugin-export && poetry config warnings.export false

# Set the working directory to /app within the container
WORKDIR /app

# Copy the pyproject.toml and poetry.lock* files to the /app directory in the container
# The poetry.lock* allows for an optional lock file, accommodating projects without one
COPY pyproject.toml  poetry.lock* /app/

# Export the dependencies from poetry to a requirements.txt file without hashes
# This is needed for pip to understand and install the dependencies
RUN poetry export -f requirements.txt --output requirements.txt --without-hashes

# Install the Python dependencies specified in requirements.txt
# The `--no-cache-dir` flag prevents pip from caching installed packages, reducing image size
RUN pip install --no-cache-dir --upgrade -r /app/requirements.txt

# Copy the application source code from the local src directory to the /app directory in the container
COPY ./src/ /app/

# Define the default command to run when the container starts
# This command runs the app using uvicorn with proxy headers enabled, listening on all network interfaces on port 80
CMD ["uvicorn", "ska_ems_webhook.main:app", "--proxy-headers", "--host", "0.0.0.0", "--port", "80"]
