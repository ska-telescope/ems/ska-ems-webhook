Engineering Management System (EMS) Webhook 
===========================================

# Quick start
To clone this repository, run

```
git clone --recurse-submodules git@gitlab.com:ska-telescope/ems/ska-ems-webhook.git
```

To refresh the GitLab Submodule, execute below commands:

```
git submodule update --recursive --remote
git submodule update --init --recursive
```

### Build and test

Install dependencies with Poetry and activate the virtual environment

```
poetry install
poetry shell
```

To build a new Docker image, run

```
make oci-build
```

Execute the test suite and lint the project with:

```
make python-test
make python-lint
```

# Documentation

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-ems-webhook/badge/?version=latest)](https://developer.skao.int/projects/ska-ems-webhook/en/latest/?badge=latest)

Documentation can be found in the ``docs`` folder. To build docs, install the
documentation specific requirements:

```
pip3 install sphinx sphinx-rtd-theme recommonmark sphinxcontrib-openapi mistune==0.8.4
```

and build the documentation (will be built in docs/build folder) with

```
make docs-build html
```
